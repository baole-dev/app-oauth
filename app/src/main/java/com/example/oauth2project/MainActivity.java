package com.example.oauth2project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView username = (TextView) findViewById(R.id.username);
        TextView password = (TextView) findViewById(R.id.password);

        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);


        // authn
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(username.getText().toString())) {
                    username.setError("Username is required");
                }
                if (TextUtils.isEmpty(password.getText().toString())) {
                    password.setError("Password is required");
                }
                AuthService authn = new AuthService(username.getText().toString(), password.getText().toString());
                String token = null;
                try {
                    token = authn.onAuthn();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("Check token <> "+ token);
            }
        });

    }

}