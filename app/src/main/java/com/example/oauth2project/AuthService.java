package com.example.oauth2project;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthService {
    String username, password;
    AuthService (String username, String password){
        this.username = username;
        this.password = password;
    }

    String onAuthn() throws IOException {
        System.out.println("Hello World!"+ this.username +"/"+ this.password);
        try {
            System.out.println("Hear 1111 <><>");
            URL url = new URL("https://auth.ghtklab.com/api/v1/oauth2/token");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");

            JSONObject payload = new JSONObject();

            payload.put("username", this.username);
            payload.put("password", this.password);
            payload.put("grant_type","password");
            payload.put("client_id","01GKG6EBP6THHQD0GERS06Q206");
            payload.put("scope","auth:account.read offline_access openid");

            System.out.println("Hear 2222 <><>");
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            OutputStreamWriter wr = new OutputStreamWriter(os);
            wr.write(payload.toString());

            os.flush();

            int responseCode = conn.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                System.out.println(response.toString());
            } else {
                System.out.println("POST request did not work.");
            }
        } catch (MalformedURLException | JSONException e) {
            System.out.println("Exception <><>");
            e.printStackTrace();
        }
        return "";
    }

}
